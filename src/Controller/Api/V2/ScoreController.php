<?php declare(strict_types = 1);

namespace App\Controller\Api\V2;

use App\Controller\BaseController;
use App\Entity\Term;
use App\Service\ScoreSourceFactory;
use App\Service\Serializer\JsonApiSerializer;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcher;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\JsonResponse;

class ScoreController extends BaseController
{
    /**
     * @Get("/score/{searchTerm}", name="show")
     * @QueryParam(name="source", nullable=true)
     */
    public function show(
        string $searchTerm,
        JsonApiSerializer $jsonApiSerializer,
        ParamFetcher $paramFetcher,
        ScoreSourceFactory $scoreSourceFactory
    ): JsonResponse {
        //When new sources are added -> findByNameAndSource
        /** @var Term $term */
        $term = $this->getRepository(Term::class)->findOneByName($searchTerm);

        if (null === $term) {
            $source = $paramFetcher->get('source');

            try {
                /** @var Term $term */
                $term = $scoreSourceFactory->create($source)->getTermScore($searchTerm);
            } catch (GuzzleException $e) {
                return $this->json([
                    'error' => $e->getMessage()
                ], 400);
            }

            $this->getManager()->persist($term);
            $this->getManager()->flush();
        }

        return $this->json(
            $jsonApiSerializer->transform($term)
        );
    }
}
