<?php declare(strict_types = 1);

namespace App\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BaseController extends AbstractController
{
    protected function getManager(): ObjectManager
    {
        return $this->getDoctrine()->getManager();
    }

    protected function getRepository(string $repository): ObjectRepository
    {
        return $this->getDoctrine()->getRepository($repository);
    }
}
