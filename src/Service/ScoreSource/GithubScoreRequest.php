<?php declare(strict_types = 1);

namespace App\Service\ScoreSource;

use App\Entity\Term;
use App\Service\AbstractScoreRequest;
use App\Service\ScoreSourceFactory;
use stdClass;
use Symfony\Bundle\FrameworkBundle\Tests\CacheWarmer\testRouterInterfaceWithoutWarmebleInterface;

class GithubScoreRequest extends AbstractScoreRequest
{
    private const ENDPOINT = "https://api.github.com/search/issues";

    /** @var string */
    private $params;

    protected function getEndpoint(): string
    {
        return self::ENDPOINT . $this->params;
    }

    protected function setQueryParams(string $term): void
    {
        $this->params = "?q=" . $term;
    }

    protected function getTerm(float $score, string $searchTerm): Term
    {
        $term = new Term();

        $term->setName($searchTerm);
        $term->setScore($score);
        $term->setSource(ScoreSourceFactory::GITHUB);

        return $term;
    }


    protected function calculateScore(stdClass $positiveScore, stdClass $negativeScore): float
    {
        $sum = $positiveScore->total_count+$negativeScore->total_count;

        if (empty($sum)) {
            return $sum;
        }

        $score = round(
            ($positiveScore->total_count*10+$negativeScore->total_count*1) / ($sum),
            2)
        ;

        return $score;
    }
}
