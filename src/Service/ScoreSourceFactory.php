<?php declare(strict_types = 1);

namespace App\Service;

use App\Service\ScoreSource\GithubScoreRequest;

class ScoreSourceFactory
{
    const GITHUB = "github";
    const TWITTER = "twitter";

    public function create(?string $source): AbstractScoreRequest
    {
        $source = strtolower($source ?? '');

        switch ($source) {
            case self::GITHUB:
                return $this->createGithubScoreRequest();
            case self::TWITTER: //implement TWITTER
            default:
                return $this->createGithubScoreRequest();
        }
    }

    private function createGithubScoreRequest(): AbstractScoreRequest
    {
        return new GithubScoreRequest();
    }
}
