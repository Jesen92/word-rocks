<?php declare(strict_types = 1);

namespace App\Service\Serializer;

use App\Entity\Term;

class JsonApiSerializer
{
    public function transform(Term $term): array
    {
        return [
            "data" => [
                "id" => $term->getId(),
                "type" => 'term',
                "attributes" => $this->prepareAttributes($term),
            ],
        ];
    }

    private function prepareAttributes(Term $term): array
    {
        return [
            'term' => $term->getName(),
            'score' => $term->getScore(),
        ];
    }
}
