<?php declare(strict_types = 1);

namespace App\Service\Serializer;

use App\Entity\Term;

class JsonSerializer
{
    public function transform(Term $term): array
    {
        return [
            'term' => $term->getName(),
            'score' => $term->getScore(),
        ];
    }
}
