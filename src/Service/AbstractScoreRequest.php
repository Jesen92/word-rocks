<?php declare(strict_types = 1);

namespace App\Service;

use App\Entity\Term;
use GuzzleHttp\Exception\GuzzleException;
use stdClass;

abstract class AbstractScoreRequest
{
    protected const POSITIVE_SCORE = 'rocks';
    protected const NEGATIVE_SCORE = 'sucks';

    protected $response;

    /** @var string */
    protected $score;

    /** @var  */
    protected $term;

    /**
     * @throws GuzzleException
     */
    public function getTermScore(string $searchTerm): Term
    {
        $this->setQueryParams($searchTerm);

        $client = new \GuzzleHttp\Client();

        $positiveScoreResponse = $client->request(
            'GET',
            $this->getEndpoint() . '+' . self::POSITIVE_SCORE
        );
        $negativeScoreResponse = $client->request(
            'GET',
            $this->getEndpoint() . '+' . self::NEGATIVE_SCORE
        );

        $positiveScore = json_decode($positiveScoreResponse->getBody()->getContents());
        $negativeScore = json_decode($negativeScoreResponse->getBody()->getContents());

        $score = $this->calculateScore($positiveScore, $negativeScore);

        return $this->getTerm($score, $searchTerm);;
    }

    abstract protected function getEndpoint(): string;

    abstract protected function setQueryParams(string $term): void;

    abstract protected function getTerm(float $score, string $searchTerm): Term;

    abstract protected function calculateScore(stdClass $positiveScore, stdClass $negativeScore): float;
}
