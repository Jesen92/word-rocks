# Word rocks

'Word rocks' is an app for finding out how much some 'Word' rocks (or sucks)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install before you can start with the project setup

```
docker (https://www.docker.com/get-docker)
```

### Installing

A step by step series of examples that tell you have to get a development env running

Go into the project folder
```
cd /word-rocks
```
After that open the docker folder
```
cd docker
```
Inside the docker folder run the **build.sh** script
```
./build.sh 
//or on mac 'bash build.sh'
```

Inside your rc file add (.bashrc, .zshrc...):

```
export WORD_ROCKS_APP_PORT=8080
export WORD_ROCKS_APP_DOMAIN=wordrocks.localhost
export WORD_ROCKS_DB_PORT=3360
export WORD_ROCKS_DB_ROOT_PASS=secret
export WORD_ROCKS_DB_NAME=wordrocks
export WORD_ROCKS_DB_USER=symfony
export WORD_ROCKS_DB_PASS=secret
export WORD_ROCKS_DOCKER_MOUNT=wordrocks-sync:/var/www/html/:nocopy
export SYNC_STRATEGY=native_osx
```

In the root of the project copy .env.dist to .env
```
cp .env.dist .env
```    

###ProTips

To run dev script easier add alias to your rc file

linux:
```
alias dev="./dev"
```

mac:
```
alias dev="bash dev"
```

### Starting and stopping docker

In the root of the project

to start:
```
dev up
```
to stop:
```
dev down
```

### Final steps

Run composer install

```
dev composer install
```

Run migrations

```
dev console d:m:m 
//or dev console doctrine:migrations:migrate
```

And that's all folks

### API
{searchTerm} = term you want to find out the score for

**V1:** \
GET localhost:8080/api/v1/score/{searchTerm}

Response
```
{
    "term": "php",
    "score": 3.37
}
```

**V2:** \
GET localhost:8080/api/v2/score/{searchTerm}

Response
```
{
    "data": {
        "id": 1,
        "type": "term",
        "attributes": {
            "term": "php",
            "score": 3.37
        }
    }
}
```

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
